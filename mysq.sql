CREATE USER 'stats'@'localhost' IDENTIFIED BY 'wildwest';
GRANT ALL PRIVILEGES ON * . * TO 'stats'@'localhost';
FLUSH PRIVILEGES;


CREATE TABLE `work` (  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,  `processed` int(10) unsigned NOT NULL DEFAULT 1,  PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;

insert into work (id,processed) values(1,1);

update work set processed = 1 where id = 1;

CREATE DATABASE stats
DEFAULT CHARACTER SET utf8
DEFAULT COLLATE utf8_unicode_ci;


CREATE TABLE `cssplayers` (
	`id` mediumint unsigned NOT NULL AUTO_INCREMENT,
	`steamId` varchar(20) NOT NULL DEFAULT '',
	`nick` varchar(30) NOT NULL DEFAULT '',
	`kills` mediumint unsigned NOT NULL DEFAULT 0,
	`deaths` mediumint unsigned NOT NULL DEFAULT 0,
	`heads` smallint unsigned NOT NULL DEFAULT 0,
	`points` mediumint unsigned NOT NULL DEFAULT 0,
	PRIMARY KEY (`id`)
);

-- COD INIT

drop table codplayers;

CREATE TABLE `codplayers` (
	`id` mediumint unsigned NOT NULL AUTO_INCREMENT,
	`guid` int(12) NOT NULL DEFAULT 0,
	`nick` varchar(30) NOT NULL DEFAULT '',
	`kills` mediumint unsigned NOT NULL DEFAULT 0,
	`deaths` mediumint unsigned NOT NULL DEFAULT 0,
	`heads` smallint unsigned NOT NULL DEFAULT 0,
	`points` mediumint unsigned NOT NULL DEFAULT 0,
	`rank` mediumint unsigned NOT NULL DEFAULT 0,
	PRIMARY KEY (`id`)
);

ALTER TABLE codplayers ADD UNIQUE uGuid (guid);

--COD INIT

CREATE TABLE `codinfo` (
	`id` mediumint unsigned NOT NULL AUTO_INCREMENT,
	`lastTimeStamp` int(11) NOT NULL DEFAULT 0,
	`frags` int(12) NOT NULL DEFAULT 0,
	PRIMARY KEY (`id`)
);

insert into codinfo (id) values(1);

-- процедура - код-ранк

delimiter //

drop PROCEDURE if exists codrank ;
CREATE PROCEDURE codrank ()
BEGIN


DECLARE i INT default 0;
DECLARE length INT default 0;

DROP TABLE IF EXISTS codplayers_mirror;
CREATE TABLE codplayers_mirror AS SELECT * from codplayers ORDER BY points DESC;

-- Создадим временную таблицу с тему у кого есть рейт 1100+
DROP TABLE IF EXISTS codplayers_tmp;
SET @row = 0;
CREATE TABLE codplayers_tmp AS SELECT id,points, @row := @row + 1 AS rank from codplayers_mirror;

	SET i = 0;
	

	SELECT COUNT(*) from codplayers_tmp INTO length ;
	-- SET length = 10;


	REPEAT UPDATE codplayers_mirror SET `rank` = (SELECT `rank` from codplayers_tmp limit i,1) where `id` = (SELECT `id` from codplayers_tmp limit i,1); SET i = i + 1; UNTIL i = length END REPEAT;

	DROP TABLE codplayers_tmp;
	alter table codplayers_mirror order by rank asc;
	

	DROP TABLE IF EXISTS codplayers;
	RENAME TABLE codplayers_mirror to codplayers;

END; //
delimiter ;

-- конец процедуры кодранк


CREATE TABLE `ragnaros_core` (  `systemId` mediumint(7) unsigned NOT NULL AUTO_INCREMENT,  `name` varchar(12) NOT NULL DEFAULT '',  `realmId` varchar(30) NOT NULL DEFAULT '',  `realmName` varchar(30) NOT NULL DEFAULT '', `guildName` varchar(50) NOT NULL DEFAULT '', `guildRealm` varchar(30) NOT NULL DEFAULT '',  `genderId` tinyint(3) unsigned NOT NULL DEFAULT '0',  `classId` tinyint(2) unsigned NOT NULL DEFAULT '0',  `raceId` tinyint(3) unsigned NOT NULL DEFAULT '0', `factionId` tinyint(1) unsigned NOT NULL DEFAULT 2,  `spec1` varchar(40) NOT NULL DEFAULT '',  `spec2` varchar(40) NOT NULL DEFAULT '', `activeSpec` varchar(40) NOT NULL DEFAULT '',  `wins2v2` smallint(5) unsigned NOT NULL DEFAULT '0',  `losses2v2` smallint(5) unsigned NOT NULL DEFAULT '0',  `rating2v2` smallint(5) unsigned NOT NULL DEFAULT '0',  `ranking2v2` mediumint(9) NOT NULL DEFAULT '-1',  `wins3v3` smallint(5) unsigned NOT NULL DEFAULT '0',  `losses3v3` smallint(5) unsigned NOT NULL DEFAULT '0',  `rating3v3` smallint(5) unsigned NOT NULL DEFAULT '0',  `ranking3v3` mediumint(9) NOT NULL DEFAULT '-1',  `wins5v5` smallint(5) unsigned NOT NULL DEFAULT '0',  `losses5v5` smallint(5) unsigned NOT NULL DEFAULT '0',  `rating5v5` smallint(5) unsigned NOT NULL DEFAULT '0',  `ranking5v5` mediumint(9) NOT NULL DEFAULT '-1',  `winsRbg` smallint(5) unsigned NOT NULL DEFAULT '0',  `lossesRbg` smallint(5) unsigned NOT NULL DEFAULT '0',  `ratingRbg` smallint(5) unsigned NOT NULL DEFAULT '0',  `rankingRbg` mediumint(9) NOT NULL DEFAULT '-1',  `honorkills` mediumint(8) unsigned NOT NULL DEFAULT '0',  `logout` char(10) DEFAULT NULL,  `thumbnail` varchar(50) NOT NULL DEFAULT '',  PRIMARY KEY (`systemId`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;

ALTER TABLE ragnaros_core ADD UNIQUE uName (name,realmId);

CREATE TABLE `ragnaros_guilds` (
	`id` mediumint(7) unsigned NOT NULL AUTO_INCREMENT,
	`guildName` varchar(50) NOT NULL DEFAULT '',
	`realmName` varchar(30) NOT NULL DEFAULT '',
	PRIMARY KEY (`id`)
); 

ALTER TABLE ragnaros_guilds ADD UNIQUE uName (guildName,realmName);


CREATE TABLE `ragnaros_2v2` (  `systemId` mediumint(7) unsigned NOT NULL AUTO_INCREMENT,  `name` varchar(12) NOT NULL DEFAULT '',  `realmId` varchar(30) NOT NULL DEFAULT '',  `realmName` varchar(30) NOT NULL DEFAULT '',  `genderId` tinyint(3) unsigned NOT NULL DEFAULT '0',  `classId` tinyint(2) unsigned NOT NULL DEFAULT '0',  `raceId` tinyint(3) unsigned NOT NULL DEFAULT '0',  `spec1` varchar(40) NOT NULL DEFAULT '',  `spec2` varchar(40) NOT NULL DEFAULT '',  `wins2v2` smallint(5) unsigned NOT NULL DEFAULT '0',  `losses2v2` smallint(5) unsigned NOT NULL DEFAULT '0',  `rating2v2` smallint(5) unsigned NOT NULL DEFAULT '0',  `ranking2v2` mediumint(9) NOT NULL DEFAULT '-1', `logout` char(10) DEFAULT NULL,  `thumbnail` varchar(50) NOT NULL DEFAULT '',  PRIMARY KEY (`systemId`),  KEY `ixName` (`name`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ragnaros_3v3` (  `systemId` mediumint(7) unsigned NOT NULL AUTO_INCREMENT,  `name` varchar(12) NOT NULL DEFAULT '',  `realmId` varchar(30) NOT NULL DEFAULT '',  `realmName` varchar(30) NOT NULL DEFAULT '',  `genderId` tinyint(3) unsigned NOT NULL DEFAULT '0',  `classId` tinyint(2) unsigned NOT NULL DEFAULT '0',  `raceId` tinyint(3) unsigned NOT NULL DEFAULT '0',  `spec1` varchar(40) NOT NULL DEFAULT '',  `spec2` varchar(40) NOT NULL DEFAULT '',  `wins3v3` smallint(5) unsigned NOT NULL DEFAULT '0',  `losses3v3` smallint(5) unsigned NOT NULL DEFAULT '0',  `rating3v3` smallint(5) unsigned NOT NULL DEFAULT '0',  `ranking3v3` mediumint(9) NOT NULL DEFAULT '-1', `logout` char(10) DEFAULT NULL,  `thumbnail` varchar(50) NOT NULL DEFAULT '',  PRIMARY KEY (`systemId`),  KEY `ixName` (`name`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ragnaros_5v5` (  `systemId` mediumint(7) unsigned NOT NULL AUTO_INCREMENT,  `name` varchar(12) NOT NULL DEFAULT '',  `realmId` varchar(30) NOT NULL DEFAULT '',  `realmName` varchar(30) NOT NULL DEFAULT '',  `genderId` tinyint(3) unsigned NOT NULL DEFAULT '0',  `classId` tinyint(2) unsigned NOT NULL DEFAULT '0',  `raceId` tinyint(3) unsigned NOT NULL DEFAULT '0',  `spec1` varchar(40) NOT NULL DEFAULT '',  `spec2` varchar(40) NOT NULL DEFAULT '',  `wins5v5` smallint(5) unsigned NOT NULL DEFAULT '0',  `losses5v5` smallint(5) unsigned NOT NULL DEFAULT '0',  `rating5v5` smallint(5) unsigned NOT NULL DEFAULT '0',  `ranking5v5` mediumint(9) NOT NULL DEFAULT '-1', `logout` char(10) DEFAULT NULL,  `thumbnail` varchar(50) NOT NULL DEFAULT '',  PRIMARY KEY (`systemId`),  KEY `ixName` (`name`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ragnaros_rbg` (  `systemId` mediumint(7) unsigned NOT NULL AUTO_INCREMENT,  `name` varchar(12) NOT NULL DEFAULT '',  `realmId` varchar(30) NOT NULL DEFAULT '',  `realmName` varchar(30) NOT NULL DEFAULT '',  `genderId` tinyint(3) unsigned NOT NULL DEFAULT '0',  `classId` tinyint(2) unsigned NOT NULL DEFAULT '0',  `raceId` tinyint(3) unsigned NOT NULL DEFAULT '0',  `spec1` varchar(40) NOT NULL DEFAULT '',  `spec2` varchar(40) NOT NULL DEFAULT '',  `winsrbg` smallint(5) unsigned NOT NULL DEFAULT '0',  `lossesrbg` smallint(5) unsigned NOT NULL DEFAULT '0',  `ratingrbg` smallint(5) unsigned NOT NULL DEFAULT '0',  `rankingrbg` mediumint(9) NOT NULL DEFAULT '-1', `logout` char(10) DEFAULT NULL,  `thumbnail` varchar(50) NOT NULL DEFAULT '',  PRIMARY KEY (`systemId`),  KEY `ixName` (`name`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `ragnaros_hk` (  `systemId` mediumint(7) unsigned NOT NULL AUTO_INCREMENT,  `name` varchar(12) NOT NULL DEFAULT '',  `realmId` varchar(30) NOT NULL DEFAULT '',  `realmName` varchar(30) NOT NULL DEFAULT '',  `genderId` tinyint(3) unsigned NOT NULL DEFAULT '0',  `classId` tinyint(2) unsigned NOT NULL DEFAULT '0',  `raceId` tinyint(3) unsigned NOT NULL DEFAULT '0',  `spec1` varchar(40) NOT NULL DEFAULT '',  `spec2` varchar(40) NOT NULL DEFAULT '',  `winshk` smallint(5) unsigned NOT NULL DEFAULT '0',  `losseshk` smallint(5) unsigned NOT NULL DEFAULT '0',  `ratinghk` smallint(5) unsigned NOT NULL DEFAULT '0',  `rankinghk` mediumint(9) NOT NULL DEFAULT '-1', `logout` char(10) DEFAULT NULL,  `thumbnail` varchar(50) NOT NULL DEFAULT '',  PRIMARY KEY (`systemId`),  KEY `ixName` (`name`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `ragnaros_graph` (  `id` mediumint(7) unsigned NOT NULL AUTO_INCREMENT, `systemId` mediumint(7) unsigned NOT NULL UNIQUE,  `2v2` text(1500) NOT NULL DEFAULT '',`3v3` text(1500) NOT NULL DEFAULT '',`5v5` text(1500) NOT NULL DEFAULT '',`rbg` text(1500) NOT NULL DEFAULT '', PRIMARY KEY (`id`),  KEY `ixId` (`systemId`)) ENGINE=InnoDB DEFAULT CHARSET=utf8;



SELECT *,
IF (@score=ui.rating2v2, @rank:=@rank, @rank:=@rank+1) rank,
@score:=ui.rating2v2 score
FROM ru ui,
(SELECT @score:=0, @rank:=0) r
ORDER BY rating2v2 DESC


SELECT name,rating2v2, IF (@score=ui.rating2v2, @rank:=@rank, @rank:=@rank+1) rank, @score:=ui.rating2v2 score FROM ru ui, (SELECT @score:=0, @rank:=0) r ORDER BY rating2v2 DESC limit 10;

CREATE TEMPORARY TABLE ru_tmp1 AS SELECT systemId, IF (@score=ui.rating2v2, @rank:=@rank, @rank:=@rank+1) rank, @score:=ui.rating2v2 score FROM ru ui, (SELECT @score:=0, @rank:=0) r WHERE rating2v2 > 1100 ORDER BY rating2v2 DESC;


drop table ru_temp; create table ru_temp like ru;
alter table ru_temp add rank2v2 mediumint after rating2v2;

#функции агрегации
AVG(), COUNT(), SUM(), MIN(), MAX(), etc...

# группировка 
SELECT A,B, SUM(C)
FROM table
GROUP BY A,B WITH ROLLUP
HAVING course IS NULL;

#порядок использования клаузул
WHERE
GROUP BY
HAVING
ORDER BY
LIMIT

#процедуры 1 
drop PROCEDURE if exists p ;
delimiter //

CREATE PROCEDURE p (OUT a INT)
BEGIN
SELECT COUNT(*) from WOW.ru INTO a;
SET a = a + 1;
END;
//



delimiter ;
call p(@count);
select @count;


 SET @t1 =CONCAT('SELECT * FROM ',realmName );
 PREPARE stmt3 FROM @t1;
 EXECUTE stmt3;
 DEALLOCATE PREPARE stmt3;

-- процедуры - core_pvp
delimiter //

drop PROCEDURE if exists er_core_pvp ;
CREATE PROCEDURE er_core_pvp ()
BEGIN

DROP TABLE IF EXISTS er_core_pvp;
CREATE TABLE er_core_pvp AS SELECT * from er_core WHERE rating2v2 >= 192 or rating3v3 >= 192 or rating5v5 >= 192 or ratingrbg >= 192;

END; //
delimiter ;

-- конец процедуры core_pvp


-- процедуры - 2v2

delimiter //

drop PROCEDURE if exists er_2v2 ;
CREATE PROCEDURE er_2v2 ()
BEGIN


DECLARE i INT default 0;
DECLARE length INT default 0;

DROP TABLE IF EXISTS er_2v2_mirror;
CREATE TABLE er_2v2_mirror AS SELECT * from er_core_pvp WHERE rating2v2 >= 1100 ORDER BY rating2v2 DESC;

-- Создадим временную таблицу с тему у кого есть рейт 1100+
DROP TABLE IF EXISTS er_2v2_tmp;
SET @row = 0;
CREATE TABLE er_2v2_tmp AS SELECT systemId,rating2v2, @row := @row + 1 AS rank from er_2v2_mirror;

	SET i = 0;
	

	SELECT COUNT(*) from er_2v2_tmp INTO length ;
	-- SET length = 10;


	REPEAT UPDATE er_2v2_mirror SET `ranking2v2` = (SELECT `rank` from er_2v2_tmp limit i,1) where `systemId` = (SELECT `systemId` from er_2v2_tmp limit i,1); SET i = i + 1; UNTIL i = length END REPEAT;

	DROP TABLE er_2v2_tmp;
	alter table er_2v2_mirror drop column ranking3v3;
	alter table er_2v2_mirror drop column ranking5v5;
	alter table er_2v2_mirror drop column rankingrbg;
	alter table er_2v2_mirror drop column rating3v3;
	alter table er_2v2_mirror drop column rating5v5;
	alter table er_2v2_mirror drop column ratingRbg;
	alter table er_2v2_mirror drop column wins3v3;
	alter table er_2v2_mirror drop column wins5v5;
	alter table er_2v2_mirror drop column winsRbg;
	alter table er_2v2_mirror drop column losses3v3;
	alter table er_2v2_mirror drop column losses5v5;
	alter table er_2v2_mirror drop column lossesRbg;
	alter table er_2v2_mirror drop column honorkills;
	alter table er_2v2_mirror order by ranking2v2 asc;
	

	DROP TABLE IF EXISTS er_2v2;
	RENAME TABLE er_2v2_mirror to er_2v2;





END; //
delimiter ;

-- конец процедуры 2v2

call er_2v2();

-- процедуры - 3v3

delimiter //

drop PROCEDURE if exists er_3v3 ;
CREATE PROCEDURE er_3v3 ()
BEGIN


DECLARE i INT default 0;
DECLARE length INT default 0;

DROP TABLE IF EXISTS er_3v3_mirror;
CREATE TABLE er_3v3_mirror AS SELECT * from er_core_pvp WHERE rating3v3 >= 1100 ORDER BY rating3v3 DESC;

-- Создадим временную таблицу с тему у кого есть рейт 1100+
DROP TABLE IF EXISTS er_3v3_tmp;
SET @row = 0;
CREATE TABLE er_3v3_tmp AS SELECT systemId,rating3v3, @row := @row + 1 AS rank from er_3v3_mirror;

	SET i = 0;
	

	SELECT COUNT(*) from er_3v3_tmp INTO length ;
	-- SET length = 10;


	REPEAT UPDATE er_3v3_mirror SET `ranking3v3` = (SELECT `rank` from er_3v3_tmp limit i,1) where `systemId` = (SELECT `systemId` from er_3v3_tmp limit i,1); SET i = i + 1; UNTIL i = length END REPEAT;

	DROP TABLE er_3v3_tmp;
	alter table er_3v3_mirror drop column ranking2v2;
	alter table er_3v3_mirror drop column ranking5v5;
	alter table er_3v3_mirror drop column rankingrbg;
	alter table er_3v3_mirror drop column rating2v2;
	alter table er_3v3_mirror drop column rating5v5;
	alter table er_3v3_mirror drop column ratingRbg;
	alter table er_3v3_mirror drop column wins2v2;
	alter table er_3v3_mirror drop column wins5v5;
	alter table er_3v3_mirror drop column winsRbg;
	alter table er_3v3_mirror drop column losses2v2;
	alter table er_3v3_mirror drop column losses5v5;
	alter table er_3v3_mirror drop column lossesRbg;
	alter table er_3v3_mirror drop column honorkills;
	alter table er_3v3_mirror order by ranking3v3 asc;
	

	DROP TABLE IF EXISTS er_3v3;
	RENAME TABLE er_3v3_mirror to er_3v3;





END; //
delimiter ;

-- конец процедуры 3v3

call er_3v3();


-- процедуры - 5v5

delimiter //

drop PROCEDURE if exists er_5v5 ;
CREATE PROCEDURE er_5v5 ()
BEGIN


DECLARE i INT default 0;
DECLARE length INT default 0;

DROP TABLE IF EXISTS er_5v5_mirror;
CREATE TABLE er_5v5_mirror AS SELECT * from er_core_pvp WHERE rating5v5 >= 1100 ORDER BY rating5v5 DESC;

-- Создадим временную таблицу с тему у кого есть рейт 1100+
DROP TABLE IF EXISTS er_5v5_tmp;
SET @row = 0;
CREATE TABLE er_5v5_tmp AS SELECT systemId,rating5v5, @row := @row + 1 AS rank from er_5v5_mirror;

	SET i = 0;
	

	SELECT COUNT(*) from er_5v5_tmp INTO length ;
	-- SET length = 10;


	REPEAT UPDATE er_5v5_mirror SET `ranking5v5` = (SELECT `rank` from er_5v5_tmp limit i,1) where `systemId` = (SELECT `systemId` from er_5v5_tmp limit i,1); SET i = i + 1; UNTIL i = length END REPEAT;

	DROP TABLE er_5v5_tmp;
	alter table er_5v5_mirror drop column ranking3v3;
	alter table er_5v5_mirror drop column ranking2v2;
	alter table er_5v5_mirror drop column rankingrbg;
	alter table er_5v5_mirror drop column rating3v3;
	alter table er_5v5_mirror drop column rating2v2;
	alter table er_5v5_mirror drop column ratingRbg;
	alter table er_5v5_mirror drop column wins3v3;
	alter table er_5v5_mirror drop column wins2v2;
	alter table er_5v5_mirror drop column winsRbg;
	alter table er_5v5_mirror drop column losses3v3;
	alter table er_5v5_mirror drop column losses2v2;
	alter table er_5v5_mirror drop column lossesRbg;
	alter table er_5v5_mirror drop column honorkills;
	alter table er_5v5_mirror order by ranking5v5 asc;
	

	DROP TABLE IF EXISTS er_5v5;
	RENAME TABLE er_5v5_mirror to er_5v5;





END; //
delimiter ;

-- конец процедуры 5v5

call er_5v5();


-- процедуры - rbg

delimiter //

drop PROCEDURE if exists er_rbg ;
CREATE PROCEDURE er_rbg ()
BEGIN


DECLARE i INT default 0;
DECLARE length INT default 0;

DROP TABLE IF EXISTS er_rbg_mirror;
CREATE TABLE er_rbg_mirror AS SELECT * from er_core_pvp WHERE ratingrbg >= 1100 ORDER BY ratingrbg DESC;

-- Создадим временную таблицу с тему у кого есть рейт 1100+
DROP TABLE IF EXISTS er_rbg_tmp;
SET @row = 0;
CREATE TABLE er_rbg_tmp AS SELECT systemId,ratingrbg, @row := @row + 1 AS rank from er_rbg_mirror;

	SET i = 0;
	

	SELECT COUNT(*) from er_rbg_tmp INTO length ;
	-- SET length = 10;


	REPEAT UPDATE er_rbg_mirror SET `rankingrbg` = (SELECT `rank` from er_rbg_tmp limit i,1) where `systemId` = (SELECT `systemId` from er_rbg_tmp limit i,1); SET i = i + 1; UNTIL i = length END REPEAT;

	DROP TABLE er_rbg_tmp;
	alter table er_rbg_mirror drop column ranking3v3;
	alter table er_rbg_mirror drop column ranking2v2;
	alter table er_rbg_mirror drop column ranking5v5;
	alter table er_rbg_mirror drop column rating3v3;
	alter table er_rbg_mirror drop column rating2v2;
	alter table er_rbg_mirror drop column rating5v5;
	alter table er_rbg_mirror drop column wins3v3;
	alter table er_rbg_mirror drop column wins2v2;
	alter table er_rbg_mirror drop column wins5v5;
	alter table er_rbg_mirror drop column losses3v3;
	alter table er_rbg_mirror drop column losses2v2;
	alter table er_rbg_mirror drop column losses5v5;
	alter table er_rbg_mirror drop column honorkills;
	alter table er_rbg_mirror order by rankingrbg asc;
	

	DROP TABLE IF EXISTS er_rbg;
	RENAME TABLE er_rbg_mirror to er_rbg;





END; //
delimiter ;

-- конец процедуры rbg

call er_rbg();


-- процедура добавления показателей для графиков

delimiter //

drop PROCEDURE if exists er_graph ;
CREATE PROCEDURE er_graph ()
BEGIN


DECLARE i INT default 0;
DECLARE length INT default 0;


	SET i = 0;
	

	SELECT COUNT(*) from er_core_pvp INTO length ;
	-- SET length = 10;


	REPEAT insert into er_graph (2v2,3v3,5v5,rbg,systemId) values ((SELECT rating2v2 from er_core_pvp limit i,1),(SELECT rating3v3 from er_core_pvp limit i,1),(SELECT rating5v5 from er_core_pvp limit i,1),(SELECT ratingrbg from er_core_pvp limit i,1),(SELECT systemId from er_core_pvp limit i,1)) on duplicate key update 2v2 = concat(2v2,',',(SELECT rating2v2 from er_core_pvp limit i,1)), 3v3 = concat(3v3,',',(SELECT rating3v3 from er_core_pvp limit i,1)), 5v5 = concat(5v5,',',(SELECT rating5v5 from er_core_pvp limit i,1)), rbg = concat(rbg,',',(SELECT ratingrbg from er_core_pvp limit i,1)); SET i = i + 1; UNTIL i = length END REPEAT;

END; //
delimiter ;

-- конец процедуры графиков

call er_graph();





	-- WHILE i < 10 DO
	-- -- 	UPDATE ru SET ranking2v2 = (SELECT score from ru_tmp1 where systemId = i) where systemId = i;
	-- 	SET i = i + 1;
	-- END WHILE;


		-- UPDATE ru SET ranking2v2 = (SELECT score from ru_tmp1 where systemId = i) where systemId = i;


SHOW PROCESSLIST; -- to see if multiple connections are running 
SHOW STATUS LIKE 'Max_used_connections'; -- to see that the high-water-mark was. 


CREATE USER 'root'@'%' IDENTIFIED BY 'tropicano';
GRANT ALL PRIVILEGES ON * . * TO 'root'@'%';
FLUSH PRIVILEGES;


select DATEDIFF(date_format(CURDATE(),'%d/%m/%y %H:%i'),date_format(timestamp(from_unixtime(logout)),'%d/%m/%y %H:%i')) from ragnaros_core limit 10;

select name,ranking2v2, datediff(curdate(),str_to_date(date_format(from_unixtime(logout),'%d/%m/%y %H:%i'),'%d/%m/%y %H:%i')) from ragnaros_2v2 limit 30;


function set