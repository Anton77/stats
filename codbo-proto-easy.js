var lineReader = require('line-reader'),
	fs = require('fs'),
	pool = require('mysql').createPool({
	  connectionLimit : 100,
	  host            : 'localhost',
	  user            : 'stats',
	  password        : 'wildwest',
	  database		  : 'stats'
	}),
	dgram = require('dgram');

var cnt = 0;
var arr = [];
var points = 1;
var lastTimeStamp = 1441435583;



function rcon(ip,port,rcon,command){
	var message = new Buffer("xxxxx"+rcon+" "+command);

	message.writeUInt8(0xff,0,1);
	message.writeUInt8(0xff,1,1);
	message.writeUInt8(0xff,2,1);
	message.writeUInt8(0xff,3,1);
	message.writeUInt8(0x00,4,1);

	//message.toString();

	var client = dgram.createSocket("udp4");
	client.send(message, 0, message.length, port, ip);
	client.on('message',function(a){
		console.log(a.toString().slice(5));
		client.close();
	});

}

function mysql_real_escape_string(str) {
    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
        switch (char) {
            case "\0":
                return "\\0";
            case "\x08":
                return "\\b";
            case "\x09":
                return "\\t";
            case "\x1a":
                return "\\z";
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\"":
            case "'":
            case "\\":
            case "%":
                return "\\"+char; // prepends a backslash to backslash, percent,
                                  // and double/single quotes
        }
    });
}

function sql(what){
	pool.getConnection(function(err, connection) {
		if(err) console.log(err);
		connection.query(what, function(err, rows, fields) {
			if(err) console.log(err,what);
		});	
	connection.release();			
	});
}

function read(log){
	lineReader.eachLine(log,function(line,last){
		if(line.slice(0,11)*1<=lastTimeStamp) return;
			else if(line[11] === 'J'){
				//XXXXXXXXXX J;55471826;17;RANCOON
				arr = line.split(';');
				sql("insert into codplayers (guid,nick) values ("+(arr[1]*1)+",'"+mysql_real_escape_string(arr[3])+"') on duplicate key update nick='"+mysql_real_escape_string(arr[3])+"'");				
			}
				else if(line[11] === 'K'){
					cnt++;
					arr = line.split(';');				
					points = 1;
					//chk
					if(arr[11] === 'MOD_HEAD_SHOT') points = 5;
					//console.log(arr[11] === 'MOD_HEAD_SHOT');
					sql("update codplayers set kills = kills + 1, points = points +"+points+((points === 5)?", heads = heads + 1":"")+" where guid = "+(arr[5]*1));
				}
					// else if(line.splice(11,14) === 'say'){
					// 	arr = line.split(';');
					// 	if(arr[4].slice(1) === 'rank'){
					// 		rcon('173.199.105.74',3104,'tropico','tell '+arr[2]+' ^1GoT ^2IT!');
					// 	}
					// }
					if(last){
						lastTimeStamp = line.slice(0,11)*1;
					}
	}).then(function(){
		console.log('done: ',cnt);

	});	
}

read('allDays');