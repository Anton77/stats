var Obj = function(){
	this.data = {
		tmp: {
			rconInstance: null,
			rconBuffer: null,
			curlInstance: null,
			logArr: [],
			rewardPoints: 1,
			fragCounter: 0,
			strCounter: 0,
			messageId: 0
		},
		constance: {
			calcTime: 120000,
			emptyCHKTime: 110000,
			messageRotationTime: 60000,
			logRequestTime: 1100,
			playersCountTime: 60000
		},
		logURL: 'http://logs.gameservers.com/173.199.105.74:3104/02bb54c6-ac63-4a6a-bfe2-2fd5c0b40528',
		logName: 'data/downLog',
		lastTimeStamp: 0,
		totalPlayers: 0,
		totalFrags: 0,
		mainInterval: null,
		emptyCHKInterval: null,
		messageInterval: null,
		rankCalcInterval: null,
		playerCountInterval: null,
		messages: [],
		serverEmpty: 0
	}
	this.lineReader = require('line-reader');
	this.fs = require('fs');
	this.pool = require('mysql').createPool({
	  connectionLimit : 10,
	  host            : 'localhost',
	  user            : 'stats',
	  password        : 'wildwest',
	  database		  : 'stats'
	});
	this.dgram = require('dgram');
	this.Curl = require('node-libcurl').Curl;
	this.fs = require('fs');
}


Obj.prototype = {
	rcon: function(ip,port,rcon,command,callback){
		var that = this;
		this.data.tmp.rconBuffer = new Buffer("xxxxx"+rcon+" "+command);

		this.data.tmp.rconBuffer.writeUInt8(0xff,0,1);
		this.data.tmp.rconBuffer.writeUInt8(0xff,1,1);
		this.data.tmp.rconBuffer.writeUInt8(0xff,2,1);
		this.data.tmp.rconBuffer.writeUInt8(0xff,3,1);
		this.data.tmp.rconBuffer.writeUInt8(0x00,4,1);

		this.data.tmp.rconInstance = this.dgram.createSocket("udp4");
		this.data.tmp.rconInstance.send(this.data.tmp.rconBuffer, 0, this.data.tmp.rconBuffer.length, port, ip);
		this.data.tmp.rconInstance.on('message',function(data){
			if(typeof callback === 'function') callback(data);
			that.data.tmp.rconInstance.close();
		});
	},
	mysql_real_escape_string: function(str) {
	    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
	        switch (char) {
	            case "\0":
	                return "\\0";
	            case "\x08":
	                return "\\b";
	            case "\x09":
	                return "\\t";
	            case "\x1a":
	                return "\\z";
	            case "\n":
	                return "\\n";
	            case "\r":
	                return "\\r";
	            case "\"":
	            case "'":
	            case "\\":
	            case "%":
	                return "\\"+char; // prepends a backslash to backslash, percent,
	                                  // and double/single quotes
	        }
	    });
	},
	sql: function(what,callback){
		this.pool.getConnection(function(err, connection) {
			if(err) console.log(err);
			connection.query(what, function(err, rows, fields) {
				if(err) console.log(err,what);
				if(typeof callback === 'function') callback(rows);
			});	
			connection.release();			
		});
	},
	test: function(){
		var that = this;	
		console.time('mainTimer');
		that.read('allDays',function(){
			console.log('new srtrings: '+that.data.tmp.strCounter+ ' new Frags: '+that.data.tmp.fragCounter);
			console.timeEnd('mainTimer');
		});	
	},
	init: function(){
		var that = this;
		
		that.startCountPlayers();
		that.startEmptyCHK();
		that.startLogWath();
		that.startRankCalc();
		that.startMessageRotation();
	},
	startCountPlayers: function(){
		var that = this;

		that.sql('select count(*) as totall from codplayers',function(rows){
			that.data.totalPlayers = rows[0].totall;
		});
		that.sql('select frags from codinfo where id = 1',function(rows){
			that.data.totalFrags = rows[0].frags;
		});

		this.data.playerCountInterval = setInterval(function(){
			if(that.data.serverEmpty) return;
			that.sql('select count(*) as totall from codplayers',function(rows){
				that.data.totalPlayers = rows[0].totall;
			});
			that.sql('select frags from codinfo where id = 1',function(rows){
				that.data.totalFrags = rows[0].frags;
			});
		},that.data.constance.playersCountTime);
	},
	startRankCalc: function(){
		var that = this;
		this.data.rankCalcInterval = setInterval(function(){
			if(that.data.serverEmpty) return;
			console.log('---RECALC RANKS---');
			clearInterval(that.data.mainTimer);
			that.data.mainTimer = null;
			that.sql('call codrank()',function(rows){
				that.startLogWath();
			});			
		},that.data.constance.calcTime);
	},
	startEmptyCHK: function(){
		var that = this;
		this.data.emptyCHKInterval = setInterval(function(){
			that.rcon('173.199.105.74',3104,'tropico','status',function(data){
				if(((data.toString().match(/\n/g).length*1)-5)>0){
					if(that.data.serverEmpty) that.data.serverEmpty = 0;
				}
				else {
					if(!that.data.serverEmpty) that.data.serverEmpty = 1;						
				}
			});	
		},that.data.constance.emptyCHKTime);		
	},
	startMessageRotation: function(){
		var that = this;		
		that.data.messageInterval = setInterval(function(){			
			if(that.data.serverEmpty) return;
			that.data.messages = ['Tracking ^2'+that.data.totalPlayers+' ^7players with ^1'+that.data.totalFrags+' ^7frags','^7Type ^1rank ^7to check your stats'];
			that.data.tmp.messageId++;
			if(that.data.tmp.messageId > that.data.messages.length-1) that.data.tmp.messageId = 0;
			that.rcon('173.199.105.74',3104,'tropico','say '+that.data.messages[that.data.tmp.messageId]);
		},that.data.constance.messageRotationTime);
	},	
	startLogWath: function(){
		var that = this;
		//CHK FOR PLAYERS > 0
		that.data.mainTimer = setInterval(function(){
					//EMPTY FLAG CHK
					if(that.data.serverEmpty) {
						console.log('---Serv EMPTY---');
						return;
					}		
					//MAIN STRUCT - P1 DOWNLOAD LOG
					console.log('---NEW LOG REQUEST---');
					that.downloadLogFile(that.data.logURL,that.data.logName,function(){
						//P2 GET LAST TIME STAMP FROM DB
						that.getLastTimeStamp(function(){
							//P3 READ DOWNLOADED LOG
							that.read(that.data.logName,function(){
								//P4 AFTERPARTY COUT
								console.log('new strings: '+that.data.tmp.strCounter+ ' new Frags: '+that.data.tmp.fragCounter);
								console.timeEnd('mainTimer');
							});
						});
					});
		},that.data.constance.logRequestTime);		
	},	
	downloadLogFile:function(url,fileName,callback){
		var that = this;
		that.data.tmp.curlInstance = new that.Curl();
		that.data.tmp.curlInstance.setOpt('URL', url);
		that.data.tmp.curlInstance.setOpt('ACCEPT_ENCODING','gzip,deflate');
		that.data.tmp.curlInstance.on('end',function(statusCode,body,headers){
			if(statusCode === 200){
				that.fs.writeFile(fileName, body, function(err) {
			    if(err) return console.log(err);
			    	if(typeof callback === 'function') callback();   	
				}); 
			}
			else console.log('wrongStatus: '+statusCode);
		    this.close();
		});

		that.data.tmp.curlInstance.on( 'error', that.data.tmp.curlInstance.close.bind( that.data.tmp.curlInstance ) );
		that.data.tmp.curlInstance.perform();		
	},
	getLastTimeStamp: function(callback){
		console.time('mainTimer');
		var that = this;
		
		that.sql('select lastTimeStamp from codinfo where id = 1',function(rows){
			//console.log('Last TS is: '+rows[0].lastTimeStamp);
			that.data.lastTimeStamp = rows[0].lastTimeStamp;
			if(typeof callback === 'function') callback();
		});
	},
	read: function(log,callback){
		var that = this;
		that.data.tmp.fragCounter = 0;
		that.data.tmp.strCounter = 0;
		this.lineReader.eachLine(log,function(line,last){
			if(line.slice(0,11)*1<=that.data.lastTimeStamp) return;	
			else that.data.tmp.strCounter++;		
				if(line[11] === 'J'){
					//JOIN - XXXXXXXXXX J;55471826;17;RANCOON
					that.data.tmp.logArr = line.split(';');
					that.sql("insert into codplayers (guid,nick) values ("+(that.data.tmp.logArr[1]*1)+",'"+that.mysql_real_escape_string(that.data.tmp.logArr[3])+"') on duplicate key update nick='"+that.mysql_real_escape_string(that.data.tmp.logArr[3])+"'");				
				}
					//FRAG
					else if(line[11] === 'K'){
						that.data.tmp.fragCounter++;
						that.data.tmp.logArr = line.split(';');				
						that.data.tmp.rewardPoints = 1;
						//chk headshot
						if(that.data.tmp.logArr[11] === 'MOD_HEAD_SHOT') that.data.tmp.rewardPoints = 5;
						//FOR VICTIM
						that.sql("update codplayers set deaths = deaths + 1 where guid = "+that.data.tmp.logArr[1]);
						//FOR KILLER
						that.sql("update codplayers set kills = kills + 1, points = points +"+that.data.tmp.rewardPoints+((that.data.tmp.rewardPoints === 5)?", heads = heads + 1":"")+" where guid = "+(that.data.tmp.logArr[5]*1));
					}
						//CHAT
						else if(line.slice(11,14) === 'say'){
							that.data.tmp.logArr = line.split(';');
							if(that.data.tmp.logArr[4].slice(1,5) === 'rank'){
								that.sql("select kills,deaths,heads,points,rank from codplayers where guid = "+that.data.tmp.logArr[1],function(rows){
									that.rcon('173.199.105.74',3104,'tropico','say ^1[S] ^2'+that.data.tmp.logArr[3]+' ^7[^1'+((rows[0].rank*1 === 0)?'U':rows[0].rank)+'^3/'+that.data.totalPlayers+'^7] with ^1'+rows[0].points+' ^7points "('+rows[0].kills+' frags, '+rows[0].deaths+' deaths, '+((rows[0].kills/rows[0].deaths).toFixed(2))+' ratio)"');
								});								
							}
							// else if(that.data.tmp.logArr[4].slice(1,5) === 'top5'){
							// 	that.sql('select rank,kills,nick limit 5',function(rows){
							// 		that.rcon('173.199.105.74',3104,'tropico','say 1.^2Shaman ^11024 ^7points');
							// 		that.rcon('173.199.105.74',3104,'tropico','say 2.^2Shaman ^11024 ^7points');	
							// 		});				
					
							// }
						}
			if(last){
				that.data.lastTimeStamp = line.slice(0,11)*1;
				that.sql('update codinfo set lastTimeStamp = '+that.data.lastTimeStamp+', frags = frags + '+that.data.tmp.fragCounter+' where id = 1');
			}
		}).then(function(){
			if(typeof callback === 'function') callback();			
		});	
	}	

}


//new Obj().getLastTimeStamp();
//new Obj().test();
new Obj().init();