var id = 0;
var Stats = function(){
	var that = this;
	this.data = {
		awards: {
			points: 1
		},
		regex: {
			steam: /(?:<(STEAM_[\w|\d|:]+|BOT)>)/,
			nick: /(?:"([^<]+))/,
			nick2: /(?:killed\s"([^<]+))/,
			headshot: /(?:\((headshot)\))/,
			console: /(?:"(Console)<)/
		},
		tmp: {
			steam: '',
			steam2: '',
			nick: '',
			nick2: '',
			headshot: 0,
			log: '',
			console: '',
			arr: [],
			rcon: {}
		}
	};	
	this.query = require('game-server-query');
	this.SourceCon = require("sourcecon");
    this.server = require('dgram').createSocket('udp4');
	this.pool = require('mysql').createPool({
	  connectionLimit : 10,
	  host            : 'localhost',
	  user            : 'stats',
	  password        : 'wildwest',
	  database		  : 'stats'
	});		
	this.server.on('message', function (message, rinfo) {
			console.log('message:',id++);
			that.main(message.toString('utf8').slice(5,-1));
	    });

	this.server.on('listening', function () {
	    var address = that.server.address();
	    console.log('UDP Server listening ' + address.address + ':' + address.port);
	});
	this.server.bind(8006,'192.168.0.18');  	
}

Stats.prototype = {
	main: function(log){
		var that = this;
		that.data.tmp.log = log;

		//определим тип сообщения
		//фраг
		if(that.data.tmp.log.search(/killed/)>-1){
			//победителю
			this.data.awards.points = 1;
			this.data.tmp.nick = this.data.regex.nick.exec(that.data.tmp.log)[1];
			this.data.tmp.headshot = this.data.regex.headshot.exec(that.data.tmp.log);
			that.data.tmp.steam = that.data.regex.steam.exec(that.data.tmp.log)[1];
			if(this.data.tmp.headshot !== null && this.data.tmp.headshot[1] === 'headshot') that.data.awards.points = 5;
			//БАЗА - НАЧАЛО
			that.pool.getConnection(function(err, connection) {
  				if(err) console.log(err);
				connection.query("update players set kills = kills + 1, points = points +"+that.data.awards.points+((that.data.awards.points === 5)?", heads = heads + 1":"")+" where nick = '"+that.data.tmp.nick+"' and steamId = '"+that.data.tmp.steam+"'", function(err, rows, fields) {
					if(err){console.log(err);}
				});
				connection.release();	
			});
			//БАЗА - КОНЕЦ

			//проигравшему
			this.data.tmp.nick2 = this.data.regex.nick2.exec(that.data.tmp.log)[1];
			that.data.tmp.steam2 = that.data.regex.steam.exec(that.data.tmp.log.split('"')[3])[1];
			//БАЗА - НАЧАЛО
			that.pool.getConnection(function(err, connection) {
  				if(err) console.log(err);
				connection.query("update players set deaths = deaths + 1 where nick = '"+that.data.tmp.nick2+"' and steamId = '"+that.data.tmp.steam2+"'", function(err, rows, fields) {
					if(err){console.log(err);}
				});
				connection.release();	
			});
			//БАЗА - КОНЕЦ	
		}
			//чат
			else if(that.data.tmp.log.search(/say(_team)*/)>-1){

				//console check
				this.data.tmp.console = this.data.regex.console.exec(that.data.tmp.log);
				if(this.data.tmp.console !== null && this.data.tmp.console[1] === 'Console') return;

				//сплит по ковычкам
				that.data.tmp.arr = that.data.tmp.log.split('"');

				if(that.data.tmp.arr[3].trim() === 'stats' || that.data.tmp.arr[3].trim() === 'rank'){
					that.data.tmp.steam = that.data.regex.steam.exec(that.data.tmp.log)[1];
					that.data.tmp.nick = that.data.regex.nick.exec(that.data.tmp.log)[1];
					//БАЗА - НАЧАЛО
					that.pool.getConnection(function(err, connection) {
		  				if(err) console.log(err);
						connection.query("select kills,deaths,heads,points from players where steamId = '"+that.data.tmp.steam+"' and nick ='"+that.data.tmp.nick+"'", function(err, rows, fields) {
							if(err){console.log(err);}
							//console.log(rows[0]);
							that.cmd('say '+that.data.tmp.nick+' очков: '+rows[0].points+' фрагов: '+rows[0].kills+' (в хед: '+rows[0].heads+') поражений: '+rows[0].deaths);
						});
						connection.release();	
					});
					//БАЗА - КОНЕЦ
				}
				else if(that.data.tmp.arr[3].trim() === 'top5'){
					//БАЗА - НАЧАЛО
					that.pool.getConnection(function(err, connection) {
		  				if(err) console.log(err);
						connection.query("select points,nick from players order by points desc limit 5", function(err, rows, fields) {
							if(err){console.log(err);}
							console.log(rows);
							for(var a in rows){
								that.cmd('say '+(a*1+1)+'. '+rows[a].points+' очков '+rows[a].nick);
							}
							//that.cmd('');
						});
						connection.release();	
					});
					//БАЗА - КОНЕЦ					
				}
			} 
				//новый (окончание) раунд(а)				
				else if(that.data.tmp.log.search(/Round_Start/)>-1) console.log('***NEW ROUND***',that.data.tmp.log);
					//дисконнект
					else if(that.data.tmp.log.search(/disconnected/)>-1) console.log('***DISCONNECT***',that.data.tmp.log);
						//коннект
						else if(that.data.tmp.log.search(/entered/)>-1) {
							console.log('***CONNECT***',that.data.tmp.log);

							that.data.tmp.steam = that.data.regex.steam.exec(that.data.tmp.log)[1];
							that.data.tmp.nick = that.mysql_real_escape_string(that.data.regex.nick.exec(that.data.tmp.log)[1]);

							that.pool.getConnection(function(err, connection) {
	  							if(err) console.log(err);
	  							connection.query("insert into players (steamId,nick) values ('"+that.data.tmp.steam+"','"+that.data.tmp.nick+"') on duplicate key update nick='"+that.data.tmp.nick+"'", function(err, rows, fields) {
									if(err){console.log(err);}
								});
								connection.release();
	  						});
						}
							
							else return; 

		//проверка ранка  "Shaman<2><STEAM_0:0:9977876><CT>" say "rank"
		//if(this.data.arr[5] === 'say' && this.data.arr[6].toString().trim() === '"rank"') this.cmd('say твой рейтинг номр'); 

		//фраг ищем "Elmer<21><BOT><TERRORIST>" killed "Irwin<35><BOT><CT>" with "hegrenade" (headshot)

		//сообщение ищем say | say_team  "Shaman<2><STEAM_0:0:9977876><CT>" say_team "rsds"

		//новый раунд World triggered "Round_Start"
	},
	func1: function(){},
	check: function(callback){
		this.query({type: 'css',host: '192.168.0.18'},function(state){ 
	        if(state.error)	return false;
	        return callback(state.players.length);
		});
	}, 
	cmd: function(cmd,callback){
		var that = this;
		this.data.tmp.rcon = new this.SourceCon("192.168.0.18", 27015); 
		this.data.tmp.rcon.connect(function(err) {
		    if(err) return err;
		    that.data.tmp.rcon.auth("qwerty", function(err) {
		        if(err) return err;
		        that.data.tmp.rcon.send(cmd, function(err, res) {
		            if(err) return err; 
		            if(typeof callback === 'function') callback(res);		            
		        });
		    });
		});		
	},
	mysql_real_escape_string:function(str) {
	    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
	        switch (char) {
	            case "\0":
	                return "\\0";
	            case "\x08":
	                return "\\b";
	            case "\x09":
	                return "\\t";
	            case "\x1a":
	                return "\\z";
	            case "\n":
	                return "\\n";
	            case "\r":
	                return "\\r";
	            case "\"":
	            case "'":
	            case "\\":
	            case "%":
	                return "\\"+char; // prepends a backslash to backslash, percent,
	                                  // and double/single quotes
	        }
	    });
	}
}

var stats = new Stats();

//запрос данных анонимных
//stats.check(function(a){console.log('players cnt:',a);});

//ркон запрос с ответом
//stats.cmd('say hi yo',function(a){console.log(a.toString());}); 